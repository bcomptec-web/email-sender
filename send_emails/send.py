#!/usr/bin/env python

import csv
import sys
import os

from email.utils import make_msgid
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from django.conf import settings
from django.core import mail
from django.template.loader import get_template
from django.contrib.staticfiles import finders
import django
import logging
import time
import datetime
import imaplib, email
from imapclient import IMAPClient
from uuid import uuid4
import pymysql.cursors
import requests
import argparse

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# imaplib.Debug = 5

# Usage: DJANGO_SETTINGS_MODULE=settings python send.py

# Email module
# https://docs.djangoproject.com/en/2.1/topics/email/
# EmailMultiAlternatives: https://github.com/django/django/blob/master/django/core/mail/message.py#L415
# BaseEmailBackend: https://github.com/django/django/blob/master/django/core/mail/backends/base.py
# EmailBackend: https://github.com/django/django/blob/master/django/core/mail/backends/smtp.py
# SMTPlib: https://docs.python.org/3.7/library/smtplib.html

# Configure email for development: https://docs.djangoproject.com/en/2.1/topics/email/#configuring-email-for-development
# python -m smtpd -n -c DebuggingServer localhost:1025
# This command will start a simple SMTP server listening on port 1025 of localhost.
# This server simply prints to standard output all email headers and the email body.
# You then only need to set the EMAIL_HOST and EMAIL_PORT accordingly.

def get_file_contents(filepath, default=''):
    if not os.path.exists:
        return default
    with open(filepath, 'r') as f:
        return f.read()


def get_logo():
    with open(finders.find(settings.COMPANY_LOGO_FILE), 'rb') as f:
        logo = MIMEImage(f.read())
        logo.add_header('Content-ID', '<companylogo>')
        return logo


def render_template(template, params):
    return template.render(params)


def dump_template_cache(template_cache):
    for t in template_cache:
        msg = f"{settings.EMAIL_TEMPLATES_DIR}/{t}: "
        for e in template_cache[t].keys():
            msg += f"{e} "
        logger.debug(msg)


def get_line_count(filename):
    if not os.path.exists(filename):
        return 0
    with open(filename) as f:
        return sum(1 for line in f)


def print_message(msg, full=False):
    logger.debug("Message ID: {}\nFrom: {}\nTo: {}\nSubject: {}\nDate: {}\n".format(msg.get('Message-ID'),
                                                                                    msg.get('From'),
                                                                                    msg.get('To'),
                                                                                    msg.get('Subject'),
                                                                                    msg.get('Date')))
    if full:
        if msg.is_multipart():
            for part in msg.walk():
                content_type = part.get_content_type()
                if content_type in ['text/plain', 'text/html']:
                    logger.debug(content_type, "\n", part.get_payload(decode=True).decode())
        else:
            logger.debug(msg.get_payload(decode=True).decode())


def unchaught_exception_handler(type, value, tb):
    logger.exception("Uncaught exception: {0}".format(str(value)))


sys.excepthook = unchaught_exception_handler


def load_template_cache():
    template_cache = {}
    for directory, subdirs, files in os.walk(settings.EMAIL_TEMPLATES_DIR):
        if directory == settings.EMAIL_TEMPLATES_DIR:
            continue

        template_dir = os.path.basename(directory)
        for filename in files:
            ext = None
            if filename.lower().endswith('.txt'):
                ext = 'txt'
            elif filename.lower().endswith('.html'):
                ext = 'html'

            if ext:
                if template_dir not in template_cache:
                    template_cache[template_dir] = {}
                template_cache[template_dir][ext] = get_template(os.path.join(template_dir, filename))
    else:
        logger.info("Successfully loaded template cache")
        dump_template_cache(template_cache)
    return template_cache


def get_database_connection():
    return pymysql.connect(host=settings.DATABASE_HOST,
                        user=settings.DATABSE_USER,
                        password=settings.DATABASE_PASSWORD,
                        db=settings.DATABASE_NAME,
                        charset=settings.DATABASE_CHARSET,
                        cursorclass=pymysql.cursors.DictCursor)


def send_matches_to_server(payload):
    url = settings.TRACKING_URL
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, json=payload, headers=headers)
    if response.status_code == 200:
        logger.info("Successfully saved matches (uuid, email) to remote database via HTTP")
        return True
    else:
        logger.error("Failed to save matches to remote database via HTTP: {}".format(response.json()))
        return False


def mark_odoo_activity_as_done(res_id, is_followup):
    if not res_id:
      logger.warning("Cannot mark activity as done in Odoo. Resource id is empty")
      return False
    summary = 'First Email'
    if is_followup:
        summary = 'Feedback Email'

    url = f'{settings.ODOO_URL}/lead/mark_activity_as_done?&res_id={res_id}&activity_type_xml_id=mail.mail_activity_data_email&summary={summary}&feedback='
    response = requests.get(url)
    if response.status_code == 200:
        logger.info(f"Successfully marked activity: {summary} as done in Odoo! Check {settings.ODOO_URL}/web?debug=true#id={res_id}&action=149&model=crm.lead&view_type=form&menu_id=111")
        return True
    else:
        logger.error(f"Failed to mark activity as done! res_id: {res_id}, summary: {summary}, url: {url}")
        return False


def save_matches_to_database_via_http(filepath, send_type):
    logger.info("Started saving matches (uuid, email) to remote database via HTTP")
    try:
        with open(filepath, 'r', newline='', encoding='utf-8') as f:
            reader = csv.reader(f, delimiter=',', quotechar='"', skipinitialspace=True)
            next(reader)

            payload = []
            for row in reader:
                email = row[1]
                uuid = row[4]
                itime = row[5]
                payload.append({'uid': uuid, 'email': email, 'type': send_type, 'itime': itime})

            send_matches_to_server(payload)
    except Exception as ex:
         logger.exception("Cannot send data to server!")
    finally:
        logger.info("Finished saving matches (uuid, email) to remote database via HTTP")


# SELECT FROM_UNIXTIME(itime) as date, uid, ip FROM `requests` order by itime desc
def save_matches_to_database_via_mysql(filepath, send_type):
    logger.info("Started saving matches (uuid, email) to remote database via database connection")
    connection = None
    try:
        connection = get_database_connection()
        with open(filepath, 'r', newline='', encoding='utf-8') as f:
            with connection.cursor() as cursor:
                reader = csv.reader(f, delimiter=',', quotechar='"', skipinitialspace=True)
                next(reader)
                for row in reader:
                    email = row[1]
                    uuid = row[4]
                    itime = row[5]
                    sql = "INSERT INTO `matches` (`uid`, `email`, `type`, `itime`) VALUES (%s, %s, %s, %d)"
                    cursor.execute(sql, (uuid, email, send_type, itime))
            connection.commit()
    except Exception as ex:
         logger.exception("Cannot connect to database!")
    finally:
        if connection:
            connection.close()
        logger.info("Finished saving matches (uuid, email) to remote database via database connection")


def parse_cli_arguments():
    parser = argparse.ArgumentParser(description='File attachment(s)')
    parser.add_argument('-default_template', '--default-template', metavar='DEFAULT_TEMPLATE', type=str,
                        default="first_contact", help='Default email template')
    parser.add_argument('-files', '--files', metavar='FILES', type=str, nargs='*',
                        default=[], help='the file attachments')
    parser.add_argument('-subject', '--subject', metavar='SUBJECT', type=str,
                        help='the email subject')

    return parser.parse_args()


if __name__ == '__main__':
    django.setup()

    template_cache = load_template_cache()

    if not template_cache:
        logger.critical("Template cache is empty: Check paths!")
        sys.exit(1)

    if not os.path.exists(settings.EMAIL_CONTACTS_FILE):
        logger.critical("Contacts file doesn't exist: Check paths!")
        sys.exit(2)

    cli_args = parse_cli_arguments()
    file_attachments = cli_args.files
    default_template = cli_args.default_template
    subject_override = cli_args.subject
    
    logo = get_logo()

    total_count = get_line_count(settings.EMAIL_CONTACTS_FILE) - 1
    logger.info(f"Number of contacts: {total_count}")
    current_count = 0

    is_followup = False
    send_type = 'first_contact'
    with open(settings.EMAIL_CONTACTS_FILE, 'r', newline='', encoding='utf-8') as f:
        contacts_reader = csv.reader(f, delimiter=',', quotechar='"', skipinitialspace=True)

        header = next(contacts_reader)
        output_prefix = 'followup_'
        if 'date' in header:
            is_followup = True
            output_prefix = 'fwd_'

        send_type = 'followup' if is_followup else 'first_contact'

        with mail.get_connection(local_hostname=settings.DNS_NAME) as connection:
            output_file_path = output_prefix + settings.EMAIL_CONTACTS_FILE
            with open(output_file_path, 'w', newline='', encoding='utf-8') as ff:
                followup_writer = csv.writer(ff, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                followup_writer.writerow(['name', 'email', 'template', 'date', 'tracking_id', 'itime', 'message_id', 'external_id'])
                date = datetime.datetime.today().strftime('%d.%m.%Y')

                imap_client = None
                try:
                    if is_followup:
                        imap_client = IMAPClient(settings.EMAIL_HOST)
                        imap_client.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
                        imap_client.select_folder('INBOX.Sent', readonly=True)

                    for row in contacts_reader:
                        template = default_template
                        has_explicit_template = False
                        has_external_id = False
                        for name in header:
                            name = name.lower()                            
                            if name == 'external id':
                                has_external_id = True
                            if name == 'template':
                                has_explicit_template = True
                        
                        external_id = None
                        if has_explicit_template:
                            customer_name, customer_email, template = row[0].strip(), row[1].strip(), row[2].strip()
                        elif has_external_id:
                            external_id, customer_name, customer_email = row[1].strip(), row[2].strip(), row[3].strip()
                        else:
                            customer_name, customer_email = row[1].strip(), row[2].strip()

                        originalEmailId = None
                        if is_followup:
                            date = row[3].strip()
                            if len(row) >= 7:
                                originalEmailId = row[6].strip()
                            if len(row) >= 8:
                                external_id = row[7].strip()
                        current_count += 1

                        if subject_override:
                            subject = subject_override.format(customer_name)
                        elif is_followup:
                            subject = f'Fwd: New resources for {customer_name}'
                        else:
                            subject = f'New resources for {customer_name}'

                        if template not in template_cache or (
                                'txt' not in template_cache[template] and 'html' not in template_cache[template]):
                            logger.error(f"Missing templates in template cache for {template}")
                            sys.exit(3)

                        uuid = uuid4().hex
                        context = {'customer': customer_name, 'subject': subject, 'date': date, 'uuid': uuid}
                        text_content = render_template(template_cache[template]['txt'], context)
                        logger.debug("Rendering text template")

                        html_content = render_template(template_cache[template]['html'], context)
                        logger.debug("Rendering HTML template")

                        message_id = make_msgid(domain=settings.DNS_NAME)

                        if is_followup:
                            logger.info(
                                f"[{current_count}/{total_count}] Sending follow up email to: {customer_name} - "
                                f"{customer_email}, "
                                f"template: {template}, "
                                f"subject: {subject}")
                            messages = imap_client.search(['TO', customer_email])
                            logger.debug("{} messages to {}".format(len(messages), customer_email))

                            if messages:
                                msgid, data = next(iter(imap_client.fetch(messages, ['RFC822']).items()))
                                email_message = email.message_from_bytes(data[b'RFC822'])

                                originalEmailId = email_message.get('Message-ID')
                                print_message(email_message)

                                rfcmessage = MIMEBase("message", "rfc822")
                                rfcmessage.attach(email_message)

                                headers = {'Message-ID': message_id,
                                        'In-Reply-To': originalEmailId,
                                        'References': originalEmailId}
                                message = mail.EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL,
                                                                    [customer_email], [settings.DEFAULT_FROM_EMAIL],
                                                                    headers=headers, connection=connection,
                                                                    reply_to=[settings.EMAIL_REPLY_TO])
                                message.attach(logo)
                                message.attach_alternative(html_content, "text/html")
                                message.attach(rfcmessage)
                                message.send()
                            elif originalEmailId:
                                headers = {'Message-ID': message_id,
                                        'In-Reply-To': originalEmailId,
                                        'References': originalEmailId}
                                message = mail.EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL,
                                                                    [customer_email], [settings.DEFAULT_FROM_EMAIL],
                                                                    headers=headers, connection=connection,
                                                                    reply_to=[settings.EMAIL_REPLY_TO])
                                message.attach(logo)
                                message.attach_alternative(html_content, "text/html")
                                message.send()
                            else:
                                headers = {'Message-ID': message_id}
                                message = mail.EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL,
                                                                    [customer_email], [settings.DEFAULT_FROM_EMAIL],
                                                                    headers=headers, connection=connection,
                                                                    reply_to=[settings.EMAIL_REPLY_TO])
                                message.attach(logo)
                                message.attach_alternative(html_content, "text/html")
                                message.send()
                        else:
                            logger.info(
                                f"[{current_count}/{total_count}] Sending first contact email to customer: {customer_name}, "
                                f"email: {customer_email}, "
                                f"template: {template}, "
                                f"subject: {subject}")
                            headers = {'Message-ID': message_id}
                            message = mail.EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL,
                                                                  [customer_email], [settings.DEFAULT_FROM_EMAIL],
                                                                  headers=headers, connection=connection,
                                                                  reply_to=[settings.EMAIL_REPLY_TO])
                            message.mixed_subtype = 'related'
                            message.attach(logo)
                            message.attach_alternative(html_content, "text/html")
                            
                            for attachment in file_attachments:
                                message.attach_file(attachment)
                            
                            message.send()

                        itime = int(time.time())
                        followup_row = [customer_name, customer_email, 'follow_up', date, uuid, itime, message_id, external_id]
                        followup_writer.writerow(followup_row)

                        if settings.SLEEP_TIME:
                            send_matches_to_server([{'uid': followup_row[4], 'email': followup_row[1], 'type': send_type, 'itime': followup_row[5]}])
                            mark_odoo_activity_as_done(external_id, is_followup)

                        if settings.SLEEP_TIME and current_count < total_count:
                            logger.info(f"Sleeping for {settings.SLEEP_TIME} seconds")
                            time.sleep(settings.SLEEP_TIME)
                except Exception as ex:
                    logger.exception("Houston, we have a problem!")
                finally:
                    if imap_client:
                        imap_client.logout()
    if not settings.SLEEP_TIME:
        save_matches_to_database_via_http(output_file_path, send_type)
