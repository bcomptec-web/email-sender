import os

current_dir = os.path.dirname(os.path.realpath(__file__))

SLEEP_TIME = 24

DEBUG = True
TEMPLATE_DEBUG = True

EMAIL_TEMPLATES_DIR = 'email_templates'
EMAIL_CONTACTS_FILE = 'followup_contacts.csv'
DNS_NAME = 'domain.tld'
EMAIL_LOCAL_HOSTNAME = DNS_NAME
DEFAULT_FROM_EMAIL = 'FirstName LastName <user@domain.tld>'
EMAIL_HOST = '<EMAIL HOST>'
EMAIL_PORT = 465
EMAIL_INCOMING_PORT = 993
EMAIL_HOST_USER = 'user@domain.tld'
EMAIL_HOST_PASSWORD = '<PASSWORD>'
EMAIL_REPLY_TO = 'user@domain.tld'
# EMAIL_USE_TLS = True
EMAIL_USE_SSL = True
EMAIL_USE_LOCALTIME = True
EMAIL_TIMEOUT = 5
TIME_ZONE = 'Europe/Bucharest'
USE_TZ = True

DATABASE_HOST = '<DATABASE HOST>'
DATABSE_USER = '<DATABSE USER>'
DATABASE_PASSWORD = '<DATABSE PASSWORD>'
DATABASE_NAME = '<DATABASE NAME>'
DATABASE_CHARSET = 'utf8mb4'

TRACKING_URL = 'http://domain.tld/t.php'
ODOO_URL = 'http://domain.tld:8069'
COMPANY_LOGO_FILE = 'companylogo.png'

# https://docs.djangoproject.com/en/2.1/topics/email/#email-backends
# Console backend
# Instead of sending out real emails the console backend just writes the emails that would be sent to the
# standard output
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'backends.smtp.CustomEmailBackend'

SECRET_KEY = '4e&6aw+(5&cg^_!05r(&7_#dghg_pdgopq(yk)xa^bog7j)^*j'
STATICFILES_DIRS = [
    current_dir + os.sep + 'static'
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [current_dir + os.sep + EMAIL_TEMPLATES_DIR],
    },
]
