<?php

$EXCLUDED_EMAILS = "'a@b.com', 'b@c.com'";
$EXCLUDED_IP = '0.0.0.0';
$DATABASE_HOSTNAME = 'localhost';
$DATABASE_USERNAME = "user";
$DATABASE_PASSWORD = "pass";
$DATABASE_NAME = "db name";

function getClientIp()
{
        $ipkeys = array(
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP',
            'REMOTE_ADDR',
        );

        foreach ($ipkeys as $keyword) {
            if (isset($_SERVER[$keyword]) && !empty($_SERVER[$keyword])) {
                if (filter_var($_SERVER[$keyword], FILTER_VALIDATE_IP)) {
                    return $_SERVER[$keyword];
                }
            }
        }

        return null;
}

function getDbConnection()
{
	$charset = 'utf8mb4';

	$dsn = "mysql:host=${DATABASE_HOSTNAME};dbname=${DATABASE_NAME};charset=$charset";

	$options = [
	    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
	    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	    PDO::ATTR_EMULATE_PREPARES   => false,
	];

	$pdo = null;
	try {
	     $pdo = new PDO($dsn, $DATABASE_USERNAME, $DATABASE_PASSWORD, $options);
	} catch (\PDOException $e) {
	     throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}

	return $pdo;
}

function sendImageResponse()
{
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); 	
	header('Content-Type: image/gif');
	die("\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x90\x00\x00\xff\x00\x00\x00\x00\x00\x21\xf9\x04\x05\x10\x00\x00\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02\x04\x01\x00\x3b");
}

function getUserAgent()
{
    return $_SERVER['HTTP_USER_AGENT'];
}

function getCountryAndCity($ip)
{
    $country = null;
    $city = null;
    try {
        $details = json_decode(file_get_contents("http://api.ipstack.com/${ip}?access_key=83a2faac4ad2c6802cfa74e2caaa7378"));
        if (!empty($details)) 
        {
            $country = $details->country_code;
            $city = $details->city;
    
            if (empty($country))
            {
                $details = json_decode(file_get_contents("https://ipinfo.io/{$ip}/geo"));
                
                if (empty($country))
                {
                    $country = file_get_contents("http://api.hostip.info/country.php?ip=${ip}");
                    if (!empty($details))
                    {
                        $country = $details->country;
                    } 
                    else 
                    {
                        $details = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ip}"));
                        $country = $details->geoplugin_countryCode;
                    }
                }
                else {
                    $county = $details->country;
                    $city = $details->city;
                }
            } else {
                try {
                    // Double check city (1000 requests free per day)
                    $details = file_get_contents("https://ipinfo.io/{$ip}/city");
                    if (!empty($details))
                    {
                        $city = $details;
                    }
                }
                catch (Exception $e) {
                }
            }
        }
    } catch (Exception $e) {
    }

    return [$country, $city];
}

function saveRequest($db, $uid, $userAgent)
{
	$itime = time();
	$ip = getClientIp();
	list($country, $city) = getCountryAndCity($ip);
	$sql = "INSERT INTO requests(`uid`, `itime`, `ip`, `country`, `city`, `user_agent`) VALUES (?, ?, ?, ?, ?, ?)";
	$db->prepare($sql)->execute([$uid, $itime, $ip, $country, $city, $userAgent]);
}

function placeholders($text, $count=0, $separator=","){
    $result = array();
    if($count > 0){
        for($x=0; $x<$count; $x++){
            $result[] = $text;
        }
    }

    return implode($separator, $result);
}

function matchExists($db, $match)
{
    $stmt = $db->prepare("SELECT 1 FROM `matches` WHERE `uid` = ? and `email` = ?");
    $stmt->execute([$match['uid'], $match['email']]);
    $result = $stmt->fetchColumn();
    return !empty($result);
}

function getMatches($db)
{
    $stmt = $db->prepare("SELECT `email`, `ip`, `type`, from_unixtime(m.`itime`, '%d.%M.%Y %H:%i:%s') as sent_time, from_unixtime(r.`itime`, '%d.%M.%Y %H:%i:%s') as opened_time, `country`, `city`, `user_agent`
                         FROM `matches` m INNER JOIN `requests` r ON m.uid = r.uid 
                         WHERE ip != '$EXCLUDED_IP' AND email NOT IN ($EXCLUDED_EMAILS)
                         ORDER BY r.`itime` DESC, `type` ASC");
    $stmt->execute();
    return $stmt->fetchAll();
}

function getNonMatches($db)
{
    $stmt = $db->prepare("SELECT `email`, `ip`, `type`, from_unixtime(m.`itime`, '%d.%M.%Y %H:%i:%s') as sent_time, from_unixtime(r.`itime`, '%d.%M.%Y %H:%i:%s') as opened_time, `country`, `city`
                         FROM `matches` m LEFT OUTER JOIN `requests` r ON m.uid = r.uid 
                         WHERE r.uid IS NULL AND email NOT IN $EXCLUDED_EMAILS
                         ORDER BY m.`itime` DESC, `type` ASC");
    $stmt->execute();
    return $stmt->fetchAll();
}

function saveMatches($db, $matches)
{
    $datafields = ['uid', 'email', 'type', 'itime'];
    
    $db->beginTransaction();
    
    $insert_values = array();
    foreach($matches as $match){
        if (!matchExists($db, $match))
        {
            $question_marks[] = '('  . placeholders('?', sizeof($match)) . ')';
            $insert_values = array_merge($insert_values, array_values($match));
        }
    }

    if (!empty($insert_values))
    {
        $sql = "INSERT INTO matches (" . implode(",", $datafields ) . ") VALUES " . implode(',', $question_marks);

        $stmt = $db->prepare($sql);
        try {
            $stmt->execute($insert_values);
        } catch (PDOException $e){
            return [false, $e->getMessage()];
        }
    }

	$db->commit();
	
	return [true, 'Success'];
}

function json_response($message = null, $code = 200)
{
    header_remove();
    http_response_code($code);
    header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
    header('Content-Type: application/json');

    $status = array(
        200 => '200 OK',
        400 => '400 Bad Request',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
        );
    header('Status: '.$status[$code]);
 
    return json_encode(array(
        'status' => $code < 300, // success or not?
        'message' => $message
        ));
}

if (isset($_GET['i']) && !empty($_GET['i']))
{
	$uid = $_GET['i'];
	
	if (strlen($uid) != 32)
	{
        sendImageResponse();
        return;
    }
	
	$db = getDbConnection();

	if ($db)
	{	
		saveRequest($db, $uid, getUserAgent());
	}
	sendImageResponse();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && strtolower($_SERVER['CONTENT_TYPE']) == 'application/json')
{
    $json_str = file_get_contents('php://input');

    $matches = json_decode($json_str, true);
    if ($matches)
    {
        $db = getDbConnection();
        list($result, $message) = saveMatches($db, $matches);
        if ($result)
        {
            echo json_response($message, 200);
        } 
        else 
        {
            echo json_response($message, 500);
        }
    }
    else {
        echo json_response('Invalid input data', 400);
    }
}

function renderMatches($db)
{
    $matches = getMatches($db);
    if (!empty($matches))
    {
        echo "<h1>Email tracking</h1>";

        echo "<h2>Opened emails</h2>";

         echo '
            <form action="t.php?a=clear_tests" method="post">
                <input type="submit" value="Clear tests"/>
            </form>
        ';

        echo "<table style=\"border-spacing: 10px;\" border=\"2\">";
        echo "<thead>";
        echo "<tr>";
            echo "<th>Email</th>";
            echo "<th>Type</th>";
            echo "<th>IP</th>";
            echo "<th>Country</th>";
            echo "<th>City</th>";
            echo "<th>Sent time</th>";
            echo "<th>Opened time</th>";
            echo "<th>User Agent</th>";
        echo "</tr>";
        echo "</thead>";
        
        echo "<tbody>";
        foreach($matches as $match)
        {
            echo "<tr>";
            echo "<td>" . $match['email'] . "</td>";
            echo "<td>" . $match['type'] . "</td>";
            echo "<td>" . '<a target="_blank" rel="noopener noreferrer" href="https://ipinfo.io/' . $match['ip'] . '">' . $match['ip'] . "</a></td>";
            echo "<td>" . $match['country'] . "</td>";
            echo "<td>" . $match['city'] . "</td>";
            echo "<td>" . $match['sent_time'] . "</td>";
            echo "<td>" . $match['opened_time'] . "</td>";
            echo "<td>" . $match['user_agent'] . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
    } else {
        echo "No matches yet!";
    }
}

function renderNonMatches($db)
{
    $matches = getNonMatches($db);
    if (!empty($matches))
    {
        echo "<h2>Sent emails (not opened)</h2>";

        echo "<table style=\"border-spacing: 10px;\" border=\"2\">";
        echo "<thead>";
        echo "<tr>";
            echo "<th>Email</th>";
            echo "<th>Type</th>";
            echo "<th>Sent time</th>";
        echo "</tr>";
        echo "</thead>";
        
        echo "<tbody>";
        foreach($matches as $match)
        {
            echo "<tr>";
            echo "<td>" . $match['email'] . "</td>";
            echo "<td>" . $match['type'] . "</td>";
            echo "<td>" . $match['sent_time'] . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
    } else {
        echo "No matches yet!";
    }
}

// http://domain.tld/t.php?r=render_opens
if (isset($_GET['r']) && $_GET['r'] == 'render_opens')
{
    try {
    	$db = getDbConnection();
	header('Content-Type: text/html; charset=utf-8');
	renderMatches($db);
	renderNonMatches($db);
    } 
    catch (Exception $ex) {
	die("Check connection!");
    }
}

function clearTests($db)
{
    $test_emails = ['test@domain.tld'];
    $in  = str_repeat('?,', count($test_emails) - 1) . '?';

    $sql = "SELECT uid, email FROM `matches` WHERE email IN ($in)";
    $stmt = $db->prepare($sql);
    $stmt->execute($test_emails);
    $matches = $stmt->fetchAll();

    foreach($matches as $match)
    { 
        $uid = $match['uid'];
        $email = $match['email'];

        $stmt = $db->prepare("DELETE FROM `requests` WHERE uid = ?");
        $stmt->execute([$uid]);

        $stmt = $db->prepare("DELETE FROM `matches` WHERE uid = ? and email = ?");
        $stmt->execute([$uid, $email]);
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['a']) && $_GET['a'] == 'clear_tests')
{
    $db = getDbConnection();
    clearTests($db);
    header("Location: http://domain.tld/t.php?r=render_opens");
    exit;
}
?>
