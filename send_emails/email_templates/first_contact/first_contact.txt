Hello {{customer}},

My name is FIRSTNAME_LASTNAME from COMPANY_NAME.

I’m writing to you because I think {{customer}} would like to ...

Best Regards,

FIRSTNAME_LASTNAME
Managing Director
COMPANY_NAME




COMPANY_NAME
VAT ID: VAT_ID
Trade Registry: TRADE_REGISTRY
Address: FULL_ADDRESS
Email: user@domain.tld
Phone: +01 234 567 890
Website: www.domain.tld
