#!/usr/bin/env bash

set -euo pipefail

DJANGO_SETTINGS_MODULE=settings python send.py
