# Email sending

Send mass email defined in a CSV file through a SMTP server.  
The email content needs to be defined as TXT and HTML formats, inside the `email_templates` directory.  
The content format supports Django template engine syntax, e.g. for defining placeholder variables that will be replaced dynamically at runtime.  

# Dependencies
- Python 3
- Django

## Install dependencies

```
cd send_emails
python -mvenv venv
source venv/bin/activate
pip install -r requirements.txt
```

# Run

- Modify templates in `send_emails/email_templates` once  
- Insert contacts in `contacts.csv` manually or export them from a CRM like Odoo (see `contacts_exported_from_odoo_example.csv`)  
- Send emails  
    - First contact  
      - Modify `settings.py` once  
      - Send first contact using one of the methods:  
          - `DJANGO_SETTINGS_MODULE=settings python send.py`  
          - bat: `send_first_contact.bat`  
          - sh: `send_first_contact.sh`  
        After first contacts are sent, the `followup_contacts.csv` file is generated automatically and can be used by 2.2 step.  

    - Follow up  
        - Modify `settings_followup.py` once  
        - Send followup using one of the method:  
            - `DJANGO_SETTINGS_MODULE=settings_followup python send.py`  
            - bat: `send_followup.bat`  
            - sh: `send_followup.sh`  
        After followup contacts are sent, the `fwd_followup_contacts.csv` file is generated automatically and can be further used for history tracking/analytics.  
        
Note: If you want to send file attachments in the email add them as positional arguments when calling the `send.py` script.

# Settings
Configuration parameters can be specified in `settings.py` and `settings_followup.py`  
